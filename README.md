Ange copy of KArchive
=====================

Copy of KArchive from https://projects.kde.org/projects/frameworks/karchive
with no modifications.

Used as part of the stow project in order to make the build on MS-Windows and
Debian easier.

The upstream README.md file has been moved to Upstream-README.md.

Upstream repository: git://anongit.kde.org/karchive

Release
=======

Base the version number on the upstream release version.

Example of version: 5.4.0-ange.1
